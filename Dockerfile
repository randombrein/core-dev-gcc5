FROM debian:jessie
MAINTAINER Evren KANALICI <evren.kanalici@nevalabs.com>

ENV DEBIAN_FRONTEND noninteractive

# gcc5
RUN echo "deb http://ftp.debian.org/debian/ stretch main" >> /etc/apt/sources.list
RUN apt-get -qq update && apt-get -qqy --no-install-recommends install \
    g++-5           \
&& rm -rf /var/lib/apt/lists/*
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 100 --slave /usr/bin/g++ g++ /usr/bin/g++-5

ENV CC=/usr/bin/gcc-5
ENV CXX=/usr/bin/g++-5
# RUN export CC=`which gcc`
# RUN export CXX=`which g++`
